let mysql = {}
let afterExecute = {}
const xss = require('xss')

const use = (db, afterExecuteHook) => {
  mysql = db
  mysql.insert = insert

  if (afterExecuteHook) {
    afterExecute = afterExecuteHook
  }
}

const insert = async (tableName, data) => {
  let nameList = []
  let valueList = []
  for (const [key, value] of Object.entries(data)) {
    nameList.push(key)
    valueList.push(`"${xss(value)}"`)
  }
  const sql = `INSERT INTO ${tableName} (${nameList.join(', ')}) VALUES (${valueList.join(', ')})`

  const startTime = new Date().getTime()
  try {
    const result = await mysql.execute(sql)
    const time = new Date().getTime() - startTime
    afterExecute({ sql, time })
    return result
  } catch (error) {
    const time = new Date().getTime() - startTime
    afterExecute({ sql, time, error })
    throw new Error(error)
  }
}

const mysqlCRUD = { use }

module.exports = mysqlCRUD
